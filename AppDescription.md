Hello World IGB App demonstrates how an App can add a new menu item to IGB.

To run the App:

1. Install the App.
2. Select **Hello World** under **Tools** menu. 
3. Observe that a message dialog window appears on the screen.
