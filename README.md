# About

This App demonstrates how to use the IGB API to add a menu item and connect an App to IGB.

Learn how to:

* add a new menu item to the IGB window using part of the IGB API
* install an App into IGB
* build an App using Bitbucket Piplines
* distribute an App using a Bitbucket repository's **Downloads** folder URL
 
# Bitbucket piplines example

Look at the `bitbucket_pipelines.yml` in this repository. It builds the App using maven and then copies the built artifacts to the repository's Downloads folder.

If you are using Bitbucket, you can do the same. Just enable Pipelines on your repository and add a similar `bitbucket_pipelines.yml` containing a "default" pipeline. Then, when you push a new commit to your repository, 
the pipeline will run, compile your App, make an OBR index file, and copy both artifacts to your Downloads directory.

# How to run this IGB App inside IGB

* * *

## Option 1: Build it on your local computer.

* Clone the repository
* Build the App's jar by running `mvn package`
* Start IGB (lowest version indicated in pom.xml dependendies section)
* Select **Open App Manager** from the IGB **Tools** menu
* Click **Manage Repositories** button
* Use the file chooser to select the "target" directory within your cloned git repo as a new "IGB App repository".

Re-open the IGB App Manager. You should now see a new App named "Hello World" in the list of available Apps.
Install it and look for a new menu item under the **Tools** menu in IGB.

Requires:

* Java 1.8
* mvn

## Option 2: Use this repository's Downloads folder as an IGB App repository

Because this repository's **Downloads** folder contains an OBR index file (repository.xml), you can try out the Hello World IGB App from there.

* Start IGB 
* Select **Open App Manager** from the IGB **Tools** menu
* Click **Manage Repositories** button
* Use the file chooser to select the "target" directory within your cloned git repo as a new "IGB App repository".

Re-open the IGB App Manager. You should now see a new App named "Hello World" in the list of available Apps.
Install it and look for a new menu item under the **Tools** menu in IGB.
Follow the instructions above, but instead of selecting a local folder, enter this URL:

* https://bitbucket.org/[ bb_user ]/hello-world/downloads/ 

## Option 3: Use IGB App Store

Go to https://apps.bioviz.org. Select the "Hello World App" and then click "Install".