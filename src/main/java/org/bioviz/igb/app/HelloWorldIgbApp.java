package org.bioviz.igb.app;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.swing.JOptionPane;
import org.lorainelab.igb.menu.api.MenuBarEntryProvider;
import org.lorainelab.igb.menu.api.model.MenuBarParentMenu;
import org.lorainelab.igb.menu.api.model.MenuIcon;
import org.lorainelab.igb.menu.api.model.MenuItem;
import org.osgi.service.component.annotations.Component;

@Component(immediate = true)
public class HelloWorldIgbApp implements MenuBarEntryProvider {

    private String ICONPATH = "terminal.png";
    private final String myText = "Hello IGB World!";

    @Override
    public Optional<List<MenuItem>> getMenuItems() {
        MenuItem menuItem = new MenuItem(myText, (Void t) -> {
            JOptionPane.showMessageDialog(null,myText);
            return t;
        });
        // make it super-heavy to sink to bottom of the menu!
        menuItem.setWeight(1000000000); 
        try (InputStream resourceAsStream = HelloWorldIgbApp.class.getClassLoader().getResourceAsStream(ICONPATH)) {
            menuItem.setMenuIcon(new MenuIcon(resourceAsStream));
        } catch (Exception ex) {} 
        return Optional.ofNullable(Arrays.asList(menuItem));
    }

    @Override
    public MenuBarParentMenu getMenuExtensionParent() {
        return MenuBarParentMenu.TOOLS;
    }
}
